#!/bin/bash

# git install
is_exist=$(sudo yum list installed git 2>&1 | grep "No matching Packages" | wc -l)
if [ "${is_exist}" -gt 0 ]; then
 sudo yum install -y git
fi

# git config
git config --global user.email "hellomrpc@126.com"
git config --global user.name "hellomrpc"
git config --global core.fileMode false