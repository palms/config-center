#!/bin/bash

SCRIPT_ROOT=/root/script

if [ ! -d $SCRIPT_ROOT ]; then
    mkdir $SCRIPT_ROOT
fi

run_net_script(){
    url=$1
    script_name=${url##*/}
    curl -fSL ${url} -o ${SCRIPT_ROOT}/${script_name}
    source ${SCRIPT_ROOT}/${script_name}
}

# root login
run_net_script "https://gitlab.com/palms/ops/ops-config-repo/raw/master/gcp/root-login.sh"

# disk
run_net_script "https://gitlab.com/palms/ops/ops-config-repo/raw/master/gcp/disk.sh"

# hosts
run_net_script "https://gitlab.com/palms/ops/ops-config-repo/raw/master/gcp/hosts.sh"

# docker
run_net_script "https://gitlab.com/palms/ops/ops-config-repo/raw/master/gcp/docker.sh"

# git
run_net_script "https://gitlab.com/palms/ops/ops-config-repo/raw/master/gcp/git.sh"

# docker-repository
# run_net_script "https://gitlab.com/palms/ops/ops-config-repo/raw/master/gcp/docker-repository.sh"