#!/bin/bash

# root login
is_exist=$(cat /etc/ssh/sshd_config | grep "PermitRootLogin no" | wc -l)
if [ "${is_exist}" -gt 0 ]; then
 sudo perl -pi -e 's/PermitRootLogin no/PermitRootLogin yes/g' /etc/ssh/sshd_config
 sudo systemctl restart sshd.service
fi