#!/bin/bash

add_host(){
    host=$1
    ip=`echo "$host" | awk '{print $1}'`
    sed -i "/${ip}/d" /etc/hosts
    echo "$host" >> /etc/hosts
}

add_host "10.142.0.3 node1"
add_host "10.142.0.4 node2"
add_host "10.142.0.5 node3"
add_host "10.142.0.2 c2 graylog config-server"